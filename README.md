[
    {
        "question": "Which Service based Company do you like most ?",
        "option1": "Infosys",
        "option2": "Cognizant",
        "option3": "Wipro",
        "option4": "TCS"
    },
    {
        "question": "Which Product based Company do you like most ?",
        "option1": "Google",
        "option2": "Amazon",
        "option3": "Microsoft",
        "option4": "Apple"
    },
    {
        "question": "Which PSU do you like most ?",
        "option1": "IOCL",
        "option2": "NTPC",
        "option3": "BHEL",
        "option4": "Power Grid"
    },
    {
        "question": "Which Country will you prefer for On-site ?",
        "option1": "Canada",
        "option2": "USA",
        "option3": "UK",
        "option4": "Russia"
    }
]